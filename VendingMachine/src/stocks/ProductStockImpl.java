/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocks;

import products.Product;
import java.util.ArrayList;
import products.*;

/**
 *
 * @author Florin0
 */
public class ProductStockImpl implements ProductStock {

    public final ArrayList<ArrayList<Product>> productsLists = new ArrayList<ArrayList<Product>>();
    public Product[] product = new Product[25];

    public ProductStockImpl() {
        product[1] = new Product(new CocaCola());
        product[2] = new Product(new PepsiLemon());
        product[3] = new Product(new FantaCheery());
        product[4] = new Product(new Sprite());
        product[5] = new Product(new OrangeFruttiFresh());
        product[6] = new Product(new CocaColaZero());
        product[7] = new Product(new FantaZmeura());
        product[8] = new Product(new FantaKiwi());
        product[9] = new Product(new FantaOrange());
        product[10] = new Product(new FantaCapsuni());
        product[11] = new Product(new FruttiFreshPere());
        product[12] = new Product(new SocDeLamaie());
        product[13] = new Product(new SocDePortocale());
        product[14] = new Product(new FruttiFreshPiersici());
        product[15] = new Product(new FantaPiersici());
        product[16] = new Product(new SocDePiersici());
        product[17] = new Product(new SocDeCapsuni());
        product[18] = new Product(new SocDeMere());
        product[19] = new Product(new PrigatStruguri());
        product[20] = new Product(new SantalPortocale());
        product[21] = new Product(new PrigatPortocale());
        product[22] = new Product(new NesteaCapsuni());
        product[23] = new Product(new NesteaLamaie());
        product[24] = new Product(new NesteaPiersici());
        for (int i = 1; i < product.length; i++) {
            productsLists.add(this.addElements(product[i], 50));
        }
    }

    @Override
    public Product deliverProduct(Product product) {
        return product;
    }

    private ArrayList<Product> addElements(Product product, int n) {
        ArrayList<Product> productList = new ArrayList<Product>();
        int i;
        for (i = 0; i < n; i++) {
            productList.add(product);
        }
        return productList;
    }

    @Override
    public boolean isAvailable(Product product) {
        boolean availability;
        if (productsLists.get(0).contains(product)) {
            availability = true;
        } else if (productsLists.get(1).contains(product)) {
            availability = true;
        } else if (productsLists.get(2).contains(product)) {
            availability = true;
        } else if (productsLists.get(3).contains(product)) {
            availability = true;
        } else if (productsLists.get(4).contains(product)) {
            availability = true;
        } else if (productsLists.get(5).contains(product)) {
            availability = true;
        } else if (productsLists.get(6).contains(product)) {
            availability = true;
        } else if (productsLists.get(7).contains(product)) {
            availability = true;
        } else if (productsLists.get(8).contains(product)) {
            availability = true;
        } else if (productsLists.get(9).contains(product)) {
            availability = true;
        } else if (productsLists.get(10).contains(product)) {
            availability = true;
        } else if (productsLists.get(11).contains(product)) {
            availability = true;
        } else if (productsLists.get(12).contains(product)) {
            availability = true;
        } else if (productsLists.get(13).contains(product)) {
            availability = true;
        } else if (productsLists.get(14).contains(product)) {
            availability = true;
        } else if (productsLists.get(15).contains(product)) {
            availability = true;
        } else if (productsLists.get(16).contains(product)) {
            availability = true;
        } else if (productsLists.get(17).contains(product)) {
            availability = true;
        } else if (productsLists.get(18).contains(product)) {
            availability = true;
        } else if (productsLists.get(19).contains(product)) {
            availability = true;
        } else if (productsLists.get(20).contains(product)) {
            availability = true;
        } else if (productsLists.get(21).contains(product)) {
            availability = true;
        } else if (productsLists.get(22).contains(product)) {
            availability = true;
        } else if (productsLists.get(23).contains(product)) {
            availability = true;
        } else {
            availability = false;
        }
        return availability;
    }
}
