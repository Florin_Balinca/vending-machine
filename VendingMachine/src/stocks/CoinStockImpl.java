/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocks;

import business.Coin;
import exceptions.NoChangeException;
import java.util.ArrayList;

/**
 *
 * @author Florin0
 */
public class CoinStockImpl implements CoinStock {

    public Coin[] coins = new Coin[4];
    public ArrayList<ArrayList<Coin>> coinsLists = new ArrayList<ArrayList<Coin>>();

    public CoinStockImpl() {
        coins[0] = new Coin(0.1, "$");
        coins[1] = new Coin(0.5, "$");
        coins[2] = new Coin(1.0, "$");
        coins[3] = new Coin(5.0, "$");
        for (Coin coin : coins) {
            coinsLists.add(this.addElements(coin, 100));
        }
    }

    @Override
    public ArrayList<Coin> deliverChange(double change) {
        ArrayList<Coin> changeList = new ArrayList<Coin>();
        double sumElements = 0;
        if (change == 0) {
            try {
                throw new NoChangeException("Without change");
            } catch (NoChangeException ex) {
                ex.getMessage();
            }
        } else {
            for (int i = 0; i < coinsLists.get(0).size(); i++) {
                for (int j = i + 1; j <= coinsLists.get(0).size(); j++) {
                    sumElements = sumElements + coins[0].getValue();
                    if (sumElements <= change && isCoinAvailable(coins[0])) {
                        changeList.add(coins[0]);                        
                    }
                }
            }
        }
        return changeList;
    }

    @Override
    public boolean hasChange(double change) {
        return change != 0;
    }

    private ArrayList<Coin> addElements(Coin coin, int n) {
        ArrayList<Coin> coinsList = new ArrayList<Coin>();
        int i;
        for (i = 0; i < n; i++) {
            coinsList.add(coin);
        }
        return coinsList;
    }

    @Override
    public boolean isCoinAvailable(Coin coin) {
        boolean availability;
        if (this.coinsLists.get(0).contains(coin)) {
            availability = true;
        } else if (this.coinsLists.get(1).contains(coin)) {
            availability = true;
        } else if (this.coinsLists.get(2).contains(coin)) {
            availability = true;
        } else if (this.coinsLists.get(3).contains(coin)) {
            availability = true;
        } else {
            availability = false;
        }
        return availability;
    }
}
