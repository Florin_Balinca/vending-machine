/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocks;

import business.Coin;
import java.util.List;

/**
 *
 * @author Florin0
 */
public interface CoinStock {

    public List<Coin> deliverChange(double change);

    public boolean hasChange(double change);

    public boolean isCoinAvailable(Coin coin);

}
