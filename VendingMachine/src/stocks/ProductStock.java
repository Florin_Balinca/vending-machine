/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocks;

import products.Product;

/**
 *
 * @author Florin0
 */
public interface ProductStock {
    public Product deliverProduct(Product product);

    /**
     *
     * @param product
     * @return
     */
    public boolean isAvailable(Product product);
}
