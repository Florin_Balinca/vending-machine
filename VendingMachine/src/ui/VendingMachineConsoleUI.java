/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import business.Coin;
import business.Vm;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import products.Product;
import products.ProductChoice;
import stocks.CoinStockImpl;
import stocks.ProductStockImpl;

/**
 *
 * @author Florin0
 */
public class VendingMachineConsoleUI {

    public CoinStockImpl coinStock = new CoinStockImpl();
    public ProductStockImpl productStock = new ProductStockImpl();
    public Vm vm = new Vm();

    public int readIntValue() throws IOException {
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        String intValue = buff.readLine();
        int value = Integer.parseInt(intValue);
        return value;
    }

    public String readStringValue() throws IOException {
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        String value = buff.readLine();        
        return value;
    }

    public double[] readDoubleValues(int n) throws IOException {
        String[] doubleValue = new String[n];
        double[] values = new double[n];
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        int i;
        for (i = 0; i < doubleValue.length; i++) {
            doubleValue[i] = buff.readLine();
            values[i] = Double.parseDouble(doubleValue[i]);
        }
        return values;
    }

    public Coin[] readCoins(double[] value, String currency, int n) {
        Coin[] coins = new Coin[n];
        int i;
        for (i = 0; i < coins.length; i++) {
            coins[i] = new Coin(value[i], currency);
        }
        return coins;
    }

    public ArrayList<Coin> readListCoins(Coin[] coin, int n) {
        ArrayList<Coin> coinsList = new ArrayList<Coin>();
        int i;
        for (i = 0; i < n; i++) {
            if (coin[i].getValue() == coinStock.coins[0].getValue()
                    || coin[i].getValue() == coinStock.coins[1].getValue()
                    || coin[i].getValue() == coinStock.coins[2].getValue()
                    || coin[i].getValue() == coinStock.coins[3].getValue()
                    && coin[i].getCurrency().equals(coinStock.coins[i].getCurrency())) {
                coinsList.add(coin[i]);
            }
        }
        return coinsList;
    }

    public int readIndexOfProduct() throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String intValue = bf.readLine();
        int index = Integer.parseInt(intValue);
        return index;
    }

    public Object readNameObject(int i) throws IOException {
        Object product = vm.product[i].getProduct();
        return product;
    }

    public ProductChoice readProductChoice(int i) throws IOException {
        ProductChoice productChoice = new ProductChoice(readNameObject(i), vm.product[i].getPrice());
        return productChoice;
    }

    public void displayProductChoice(ProductChoice choice) {
        System.out.print("You have choosen " + choice.getProduct() + " with the price " + choice.getPrice() + coinStock.coins[0].getCurrency());
    }

    public void displayAvailableProducts(Product[] products) throws IOException {
        int i;
        System.out.println("The available products is:");
        for (i = 1; i < products.length; i++) {
            System.out.println(i + "->" + products[i].getProduct() + " with price " + products[i].getPrice() + coinStock.coins[0].getCurrency());
        }
    }
    
    public void diplayListOfProductAfterBuying(ArrayList<Product> productsList){
        System.out.println("The list of products after buying is: "+productsList);
    }
}
