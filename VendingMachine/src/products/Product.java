/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

/**
 *
 * @author Florin0
 */
public class Product {

    private double price;
    private Object product;

    private void setProduct(Object product) {
        this.product = product;
    }

    public Object getProduct() {
        return product;
    }

    public Product(Object product, double price) {
        this.setPrice(price);
        this.setProduct(product);
    }

    public Product(Object product) {
        this.setProduct(product);
    }

    public Product() {
        product = null;
        price = 0;
    }

    public double getPrice() {
        return price;
    }

    public final void setPrice(double price) {
        this.price = price;
    }
    
    @Override
    public String toString(){
        return getProduct().toString();
    }
}
