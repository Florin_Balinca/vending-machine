/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

/**
 *
 * @author Florin0
 */
public class ProductChoice extends Product {

    public ProductChoice() {
        super();
    }

    public ProductChoice(Object product) {
        super(product);
    }

    public ProductChoice(Object product, Double price) {
        super(product, price);
    }
}
