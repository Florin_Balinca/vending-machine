/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Florin0
 */
public class NoChangeException extends Exception {

    public NoChangeException(String message) {
        super(message);
    }
}
