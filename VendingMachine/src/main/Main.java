/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import business.Coin;
import business.ProductAndChange;
import business.Vm;
import java.io.IOException;
import java.util.ArrayList;
import products.Product;
import products.ProductChoice;
import ui.VendingMachineConsoleUI;

/**
 *
 * @author Florin0
 */
public class Main {

    public static void main(String[] args) {
        try {
            Vm vm = new Vm();
            VendingMachineConsoleUI console = new VendingMachineConsoleUI();
            console.displayAvailableProducts(vm.product);
            System.out.print("Given the number of coins: ");
            int readNumberOfCoins = console.readIntValue();
            System.out.print("Given the currency for each coin: ");
            String readCurrencyOfCoin = console.readStringValue();
            System.out.println("Enter coins in Vending Machine:");
            double[] readValueOfCoins = console.readDoubleValues(readNumberOfCoins);
            Coin[] coins = console.readCoins(readValueOfCoins, readCurrencyOfCoin, readNumberOfCoins);
            double sumPayed = 0;
            for (Coin coin : coins) {
                sumPayed = sumPayed + coin.getValue();
            }
            ArrayList<Coin> coinList = console.readListCoins(coins, readNumberOfCoins);
            System.out.print("The list of coins is: ");
            for (Coin coinList1 : coinList) {
                System.out.print(coinList1.getValue() + coinList1.getCurrency() + " ");
            }
            System.out.print("\nSelect a product from Vending Machine: ");
            int i = console.readIndexOfProduct();
            ProductChoice choice = console.readProductChoice(i);
            console.displayProductChoice(choice);
            ProductAndChange productAndChange = new ProductAndChange();
            if (sumPayed >= choice.getPrice()) {
                productAndChange = vm.buy(choice, coinList);
                Product removeProduct = productAndChange.getProduct();
                if (productAndChange.getProduct() != null) {
                    removeProduct = console.productStock.productsLists.get(i - 1).remove(i);                    
                }
                System.out.println("\nLoading product... " + productAndChange.getProduct());
                for (int index = 0; index < productAndChange.getChange().size(); index++) {
                    System.out.println("Loading change... " + productAndChange.getChange().get(index).getValue()
                            + productAndChange.getChange().get(index).getCurrency());
                }
                System.out.println(productAndChange.getMessage());
                System.out.println("You have purchased " + productAndChange.getProduct()
                        + " and the change is: " + productAndChange.getChange());                
            } else {
                System.out.println("\nYou couldn't buy the product " + vm.product[i].getProduct());
                ArrayList<Coin> coinsBack = vm.returnBackCoins(coinList);
                System.out.println("Take back your coins: " + coinsBack);                
            }
        } catch (IOException io) {
            System.out.println("Catching a thrown Exception from above code!!!!" + io.getMessage());
        }
    }
}
