/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.List;
import products.Product;

/**
 *
 * @author Florin0
 */
public final class ProductAndChange {

    private Product product;
    private List<Coin> change;
    private String message;

    public String getMessage() {
        return message;
    }

    public final void setMessage(String message) {
        this.message = message;
    }

    public Product getProduct() {
        return product;
    }

    public final void setProduct(Product product) {
        this.product = product;
    }

    public List<Coin> getChange() {
        return change;
    }

    public final void setChange(List<Coin> change) {
        this.change = change;
    }

    public ProductAndChange(){
        product = null;
        change = null;
        message = "";
    }
    
    public ProductAndChange(Product product, List<Coin> change, String message) {
        this.setProduct(product);
        this.setChange(change);
        this.setMessage(message);
    }

    public ProductAndChange(String message) {
        this.setMessage(message);
    }        
}
