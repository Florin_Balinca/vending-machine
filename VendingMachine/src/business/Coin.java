/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author Florin0
 */
public final class Coin {

    private String currency;
    private double value;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
   

    public Coin(double value, String currency) {
        this.currency = currency;
        this.value = value;
    } 
    
    @Override
    public String toString(){
        return Double.toString(getValue()) + this.getCurrency();
    }
}
