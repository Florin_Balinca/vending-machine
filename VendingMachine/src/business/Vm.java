/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import products.*;
import stocks.CoinStockImpl;
import stocks.ProductStockImpl;

/**
 *
 * @author Florin0
 */
public class Vm {

    public Product[] product = new Product[25];
    public final ProductStockImpl stockProduct = new ProductStockImpl();
    public final CoinStockImpl stockCoins = new CoinStockImpl();

    public Vm() {
        product[1] = new Product(new CocaCola(), 1.50);
        product[2] = new Product(new PepsiLemon(), 1.40);
        product[3] = new Product(new FantaCheery(), 1.10);
        product[4] = new Product(new Sprite(), 1.50);
        product[5] = new Product(new OrangeFruttiFresh(), 1.00);
        product[6] = new Product(new CocaColaZero(), 1.20);
        product[7] = new Product(new FantaZmeura(), 1.80);
        product[8] = new Product(new FantaKiwi(), 0.90);
        product[9] = new Product(new FantaOrange(), 1.10);
        product[10] = new Product(new FantaCapsuni(), 1.60);
        product[11] = new Product(new FruttiFreshPere(), 1.60);
        product[12] = new Product(new SocDeLamaie(), 0.70);
        product[13] = new Product(new SocDePortocale(), 0.80);
        product[14] = new Product(new FruttiFreshPiersici(), 1.60);
        product[15] = new Product(new FantaPiersici(), 1.40);
        product[16] = new Product(new SocDePiersici(), 1.20);
        product[17] = new Product(new SocDeCapsuni(), 1.10);
        product[18] = new Product(new SocDeMere(), 1.10);
        product[19] = new Product(new PrigatStruguri(), 1.50);
        product[20] = new Product(new SantalPortocale(), 1.20);
        product[21] = new Product(new PrigatPortocale(), 1.30);
        product[22] = new Product(new NesteaCapsuni(), 1.90);
        product[23] = new Product(new NesteaLamaie(), 1.50);
        product[24] = new Product(new NesteaPiersici(), 2.10);
    }

    double computeAnmount(ArrayList<Coin> credit) {
        double anmount = 0;
        for (Iterator<Coin> it = credit.iterator(); it.hasNext();) {
            anmount += it.next().getValue();
        }
        return anmount;
    }

    public ProductAndChange buy(ProductChoice choice, ArrayList<Coin> credit) {
        ProductAndChange productAndChange = new ProductAndChange();
        double creditAnmount = computeAnmount(credit);
        double changeAnmount = creditAnmount - choice.getPrice();
        boolean isProductAvailable = stockProduct.isAvailable(choice);
        Product p = choice;
        if (isProductAvailable) {
            p = stockProduct.deliverProduct(choice);
        } else {
            productAndChange = new ProductAndChange("The product is not available");
        }
        boolean hasChange = stockCoins.hasChange(changeAnmount);
        List<Coin> change = new ArrayList<Coin>();
        if (hasChange) {
            change = stockCoins.deliverChange(changeAnmount);
        } else {
            productAndChange = new ProductAndChange("No change available");
        }
        productAndChange = new ProductAndChange(p, change, "Please pickup your product and change");
        return productAndChange;
    }

    public ArrayList<Coin> returnBackCoins(ArrayList<Coin> coins) {
        return coins;
    }
}
