/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.junit.stocks;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import stocks.ProductStockImpl;

/**
 *
 * @author Florin0
 */
public class ProductStockTestClass extends TestCase {

    private final ProductStockImpl productStock = new ProductStockImpl();
    

    @BeforeClass
    public static void setUpClass() {
    }
    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    @Override
    public void setUp() {
    }

    @After
    @Override
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testIfAreProductInStock() {
        assertNotNull(productStock.productsLists);
    }
    
    @Test
    public void testIfAreCertainProductInStock(){
        boolean availableProduct = productStock.isAvailable(productStock.product[1]);
        assertTrue(availableProduct);
    }
    
    @Test
    public void testReturnProductFromACertainStock() {
        boolean productReturn = productStock.productsLists.get(2).contains(this.productStock.product[3]);
        assertEquals(true, productReturn);
    }
    
    @Test(expected = NullPointerException.class)
    public void testAvailableCertainProductInStock() {
        assertNull(this.productStock.product[0]);
    }
    
    @Test
    public void testIfStocksHaveOneHundredProductsInStock() {
        boolean result = productStock.productsLists.get(0).size() <= 100;
        assertEquals(true, result);
    }
}
