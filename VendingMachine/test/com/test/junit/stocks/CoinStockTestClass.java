/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.junit.stocks;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import stocks.CoinStock;
import stocks.CoinStockImpl;

/**
 *
 * @author Florin0
 */
public class CoinStockTestClass extends TestCase {

    private final CoinStockImpl coinStock = new CoinStockImpl();

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    @Override
    public void setUp() {
    }

    @After
    @Override
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testIfAreCoinsInStock() {
        assertNotNull(this.coinStock.coinsLists);
    }

    @Test
    public void testIfAreCertainCoinInStock() {
        boolean coinAvailable = coinStock.isCoinAvailable(coinStock.coins[1]);
        assertTrue(coinAvailable);
    }
    
    @Test
    public void testIfAreOneHundredCoinsInStock() {
        boolean numberOfCoinsAvailable = coinStock.coinsLists.get(0).size() <= 100;
        assertEquals(true, numberOfCoinsAvailable);
    }
    
    @Test
    public void testReturnCoinFromList() {
        boolean returnCoinInList = coinStock.coinsLists.get(0).contains(coinStock.coins[0]);
        assertEquals(true, returnCoinInList);
    }
    @Test
    public void testInstanceByCoinStock() {
        boolean instance = coinStock instanceof CoinStock;
        assertEquals(true, instance);
    }
}
