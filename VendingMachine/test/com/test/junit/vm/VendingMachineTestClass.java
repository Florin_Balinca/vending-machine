/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.junit.vm;

import business.Coin;
import business.ProductAndChange;
import business.Vm;
import java.util.ArrayList;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import products.CocaCola;
import products.ProductChoice;

/**
 *
 * @author Florin0
 */
public class VendingMachineTestClass extends TestCase {

    private Vm vm = new Vm();
   

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    @Override
    public void setUp() {
    }

    @After
    @Override
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testDeliverCocaCola() {
        ArrayList<Coin> coinslist = new ArrayList<Coin>();
        coinslist.add(new Coin(1.0, "$"));
        coinslist.add(new Coin(1.0, "$"));
        ProductAndChange productAndChange = new ProductAndChange();
        productAndChange = vm.buy(new ProductChoice(vm.product[1].getProduct(), vm.product[1].getPrice()), coinslist);
        assertNotNull(productAndChange.getProduct());
    }

}
