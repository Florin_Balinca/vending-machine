In this repository I posted a Java project called Vending Machine. He knows to buy a product from Vending Machine, he knows to load stocks from Vending Machine with products, coins, and finally he knows to deliver a product after what I make the selection of product and delivering the rest of money like a list of coin.

In this project, I had the following tasks:

-> create a class called Product, which defines for me a valid product for Vending Machine.
-> create a class called ProductChoice which extends the Product class, for the defining a product, choosen from Vending Machine for buying.
-> create a interface called CoinStock, which defines, for me, in this project, a stock of Coins, available for Vending Machine.
-> create a class called CoinStockImpl, which implements the functionalities of the CoinStock interface.
-> create a interface called ProductStock, which defines a stock of Products, available for Vending Machine.
-> create a class called ProductStockImpl, which implements the functionalities of the ProductStock interface.
-> create a class called Vm, which defines for me, the main entity for this app.

Also, I did make more jUNIT tests for testing many functionalities in this app.